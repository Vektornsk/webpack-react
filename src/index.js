'use strict';

import React from 'react';
import { render } from 'react-dom';

import './style.less';

import App from './components/app';

render(
    <div>
        Hello react!!
        <App />
    </div>,
    document.getElementById('root')
);