'use strict';

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {

  entry: path.join(__dirname, 'src', 'index.js'),

  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js'
  },

  module: {

    rules: [

      {
        test: /\.js$/, 
        exclude: /node_modules/, 
        use: ['react-hot-loader','babel-loader'] 
      },

      {
        test: /\.less$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader','autoprefixer-loader?browsers=last 5 versions', 'less-loader?resolve url']
        })
      },

      {
        test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
        use: 'file-loader?name=../[path][name].[ext]'
      }

    ]

  },

  devServer: {
    contentBase: path.join(__dirname, "public"),
    host: 'localhost',
    port: 8080
  },

  plugins: [
    new ExtractTextPlugin('styles.css')
  ]

};
